<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Page;
Use App\Service;
use App\Portfolio;
use App\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class IndexController extends Controller
{
    public function execute(Request $request)
    {   

        // if($request->isMethod('post')) {

        //     $messages = [

        //         'required' => 'Field :attribute must be filled',
        //         'email' => 'Field :attribute should be E-mail adress'

        //     ];

        //     $this->validate($request,[

        //         'name' => 'required|max:255',
        //         'email' =>'required|email',
        //         'text' => 'required'

        //     ], $messages);

        //     //dump($request);

        //     $data = $request->all();
            
        //     $result = Mail::send('site.email', ['data' => $data] ,function($message) use ($data) {
        //     $mail_admin = env('MAIL_ADMIN');
            
        //         $message->from($data['email'], $data['name']);
        //         $message->to($mail_admin)->subject('Question');
        //     });

        //     if ($result) {
        //         return redirect()->route('home')->with('status', 'Email is sent');
        //     }

        //}

        $pages = Page::all();
        $portfolios = Portfolio::get(array('name', 'filter', 'images'));
        $services = Service::where('id', '<', 20)->get();
        $peoples = People::take(3)->get();

        $tags = DB::table('portfolios')->distinct()->pluck('filter');



        $menu = array();
        foreach($pages as $page) {
            $item = array('title' => $page->name, 'alias' =>$page->alias );
            array_push($menu,$item);
        }

        $item =  array('title'=>'Services', 'alias' =>'service' );
        array_push($menu,$item);

        $item =  array('title'=>'Portfolio', 'alias' =>'Portfolio' );
        array_push($menu,$item);
        
        $item =  array('title'=>'Team', 'alias' =>'team' );
        array_push($menu,$item);

        $item =  array('title'=>'Contact', 'alias' =>'contact' );
        array_push($menu,$item);

        // $item =  array('title'=>'about us', 'alias' =>'aboutUs' );
        // array_push($menu,$item);


        
        
        

        return view('site.index', array(
                                        'menu' => $menu,
                                        'pages' => $pages,
                                        'services' => $services,
                                        'portfolios' => $portfolios,
                                        'peoples' => $peoples,
                                        'tags' => $tags,
                                        ));


    }
}
